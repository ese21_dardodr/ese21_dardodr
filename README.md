# Sistema Automatico de Pasteurizacion de leche

## Indice

<a name="Intro"></a>
## - Introduccion
<a name="Componentes"></a>
## - Componentes
<a name="Diagrama"></a>
## - Diagrama del sistema
<a name="Intsalación"></a>
## - Instalacion

***

## Introduccion:

<details><summary>Click para expandir</summary>

- La funcion del sistema es realizar automaticamente el proceso de pasteurizacion de leche y/o complementos alimenticios en un tanque para dejarlos listos para el consumo de terneros en galpones de cria.

- El sistema controlara la temperatura y tiempos del proceso de pasteurizacion.


</details>

***

## Componentes

<details><summary>Click para expandir</summary>
 
- Placa electronica de control modelo EDU-CIAA
- Display HMI con pantalla Táctil
- Sensor de temperatura NTC 10K
- Placa adaptadora para control de potencia
- Fuente de alimentación Switching 12V 2Amp
- Extras: Gabinete, cables, borneras, Tornilleria, etc.
Notas: Tanto el tanque de acero inoxidable como los sistemas de calefaccion, bombas, tanques, etc. son desarrollados y fabricados por la propia metalurgica.

</details>

***

## Diagrama del sistema

<details><summary>Click para expandir</summary>

![Semantic description of image](/DEB.jpg "Diagrama")

</details>

***

## Instalacion

<details><summary>Click para expandir</summary>

- La instalacion de los componentes esta a cargo de la empresa metalurgica
- Planos y Soporte de instalación a cuenta de Dardo S.A. 

</details>



