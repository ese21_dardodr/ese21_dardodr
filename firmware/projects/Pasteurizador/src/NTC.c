
#include "NTC.h"





//=================================================================================================

int16_t valor_temp_por_tabla (int16_t VALOR_TEMP_CALCULAR)
{
	/*
	//-----------TABLA DE VALORES DE LA NTC ------------------------

	En funci�n de la temperatura se muestran los valores de resistencia 
	del NTC, el voltaje en el ADC y el valor de conversi�n del ADC 
	 
	TEMP	R ntc		Valor ADC
	[�?]	[kiloOhm]							Valor ADC = (Vadc * 1023) / Vref	donde Vref=3V3
	0		27.705				752
	1		26.514523			744
	2		25.383302			735
	3		24.308019			726
	4		23.285559			716
	5		22.313				707
	6		21.385131			698
	7		20.502062			688
	8		19.661355			679
	9		18.860719			669
	10		18.098				660
	11		17.365384			650
	12		16.667254			640
	13		16.001779			630
	14		15.367236			620
	15		14.762				610
	16		14.182431			601
	17		13.629378			591
	18		13.101471			581
	19		12.597418			571
	20		12.116				561
	21		11.653616			551
	22		11.211834			541
	23		10.789615			531
	24		10.385979			522
	25		10					512
	26		9.629023			502
	27		9.274145			493
	28		8.934574			483
	29		8.609561			474
	30		8.2984				464
	31		7.998981			455
	32		7.712222			446
	33		7.437517			437
	34		7.174291			428
	35		6.922				419
	36		6.679375			410
	37		6.446737			401
	38		6.22362				393
	39		6.009581			384
	40		5.8042				376
	41		5.606145			368
	42		5.416041			360
	43		5.233526			352
	44		5.058255			344
	45		4.8899				336

	*/


	if ( VALOR_TEMP_CALCULAR >= 682 )	//VALOR ADC PARA 0�C a 10�C
	{
		if ( VALOR_TEMP_CALCULAR >= 785 )	
			return (0);
		else if ( VALOR_TEMP_CALCULAR >= 775 )	
			return (1);
		else if ( VALOR_TEMP_CALCULAR >= 766 )	
			return (2);
		else if ( VALOR_TEMP_CALCULAR >= 756 )
			return (3);
		else if ( VALOR_TEMP_CALCULAR >= 746 )	
			return (4);
		else if ( VALOR_TEMP_CALCULAR >= 735 )	
			return (5);
		else if ( VALOR_TEMP_CALCULAR >= 725 )	
			return (6);
		else if ( VALOR_TEMP_CALCULAR >= 714 )	
			return (7);
		else if ( VALOR_TEMP_CALCULAR >= 704 )	
			return (8);
		else if ( VALOR_TEMP_CALCULAR >= 693 )
			return (9);
		else if ( VALOR_TEMP_CALCULAR >= 682 )	
			return (10);
	}
	
	else if ( VALOR_TEMP_CALCULAR >= 568 )	//VALOR ADC PARA 11�C a 20�C
	{
		if ( VALOR_TEMP_CALCULAR >= 671 )	
			return (11);
		else if ( VALOR_TEMP_CALCULAR >= 660 )	
			return (12);
		else if ( VALOR_TEMP_CALCULAR >= 648 )
			return (13);
		else if ( VALOR_TEMP_CALCULAR >= 637 )	
			return (14);
		else if ( VALOR_TEMP_CALCULAR >= 626 )	
			return (15);
		else if ( VALOR_TEMP_CALCULAR >= 614 )	
			return (16);
		else if ( VALOR_TEMP_CALCULAR >= 603 )
			return (17);
		else if ( VALOR_TEMP_CALCULAR >= 591 )	
			return (18);
		else if ( VALOR_TEMP_CALCULAR >= 580 )	
			return (19);
		else if ( VALOR_TEMP_CALCULAR >= 568 )
			return (20);
	}

	else if ( VALOR_TEMP_CALCULAR >= 456 )	//VALOR ADC PARA 21�C a 30�C
	{
		if ( VALOR_TEMP_CALCULAR >= 557 )	
			return (21);
		else if ( VALOR_TEMP_CALCULAR >= 545 )	
			return (22);
		else if ( VALOR_TEMP_CALCULAR >= 534 )
			return (23);
		else if ( VALOR_TEMP_CALCULAR >= 523 )	
			return (24);
		else if ( VALOR_TEMP_CALCULAR >= 512 )	
			return (25);
		else if ( VALOR_TEMP_CALCULAR >= 500 )	
			return (26);
		else if ( VALOR_TEMP_CALCULAR >= 489 )	
			return (27);
		else if ( VALOR_TEMP_CALCULAR >= 478 )	
			return (28);
		else if ( VALOR_TEMP_CALCULAR >= 467 )
			return (29);
		else if ( VALOR_TEMP_CALCULAR >= 456 )	
			return (30);
	}
	
	else if ( VALOR_TEMP_CALCULAR >= 354 )	//VALOR ADC PARA 31�C a 40�C
	{
		if ( VALOR_TEMP_CALCULAR >= 445 )	
			return (31);
		else if ( VALOR_TEMP_CALCULAR >= 434 )	
			return (32);
		else if ( VALOR_TEMP_CALCULAR >= 424 )
			return (33);
		else if ( VALOR_TEMP_CALCULAR >= 413 )	
			return (34);
		else if ( VALOR_TEMP_CALCULAR >= 403 )	
			return (35);
		else if ( VALOR_TEMP_CALCULAR >= 393 )	
			return (36);
		else if ( VALOR_TEMP_CALCULAR >= 383 )	
			return (37);
		else if ( VALOR_TEMP_CALCULAR >= 373 )	
			return (38);
		else if ( VALOR_TEMP_CALCULAR >= 364 )
			return (39);
		else if ( VALOR_TEMP_CALCULAR >= 354 )	
			return (40);
	}
	
	else if ( VALOR_TEMP_CALCULAR >= 270 )	//VALOR ADC PARA 41�C a 50�C
	{
		if ( VALOR_TEMP_CALCULAR >= 345 )	
			return (41);
		else if ( VALOR_TEMP_CALCULAR >= 336 )	
			return (42);
		else if ( VALOR_TEMP_CALCULAR >= 327 )
			return (43);
		else if ( VALOR_TEMP_CALCULAR >= 318 )	
			return (44);
		else if ( VALOR_TEMP_CALCULAR >= 310 )	
			return (45);
		else if ( VALOR_TEMP_CALCULAR >= 301 )	
			return (46);
		else if ( VALOR_TEMP_CALCULAR >= 293 )	
			return (47);
		else if ( VALOR_TEMP_CALCULAR >= 285 )	
			return (48);
		else if ( VALOR_TEMP_CALCULAR >= 277 )
			return (49);
		else if ( VALOR_TEMP_CALCULAR >= 270 )	
			return (50);
	}
	
	else if ( VALOR_TEMP_CALCULAR >= 202 )	//VALOR ADC PARA 51�C a 60�C
	{
		if ( VALOR_TEMP_CALCULAR >= 262 )	
			return (51);
		else if ( VALOR_TEMP_CALCULAR >= 255 )	
			return (52);
		else if ( VALOR_TEMP_CALCULAR >= 248 )
			return (53);
		else if ( VALOR_TEMP_CALCULAR >= 241 )	
			return (54);
		else if ( VALOR_TEMP_CALCULAR >= 234 )	
			return (55);
		else if ( VALOR_TEMP_CALCULAR >= 227 )	
			return (56);
		else if ( VALOR_TEMP_CALCULAR >= 221 )	
			return (57);
		else if ( VALOR_TEMP_CALCULAR >= 214 )	
			return (58);
		else if ( VALOR_TEMP_CALCULAR >= 208 )
			return (59);
		else if ( VALOR_TEMP_CALCULAR >= 202 )	
			return (60);
	}
	
	else if ( VALOR_TEMP_CALCULAR >= 151 )	//VALOR ADC PARA 61�C a 70�C
	{
		if ( VALOR_TEMP_CALCULAR >= 197 )	
			return (61);
		else if ( VALOR_TEMP_CALCULAR >= 191 )	
			return (62);
		else if ( VALOR_TEMP_CALCULAR >= 186 )
			return (63);
		else if ( VALOR_TEMP_CALCULAR >= 180 )	
			return (64);
		else if ( VALOR_TEMP_CALCULAR >= 175 )	
			return (65);
		else if ( VALOR_TEMP_CALCULAR >= 170 )	
			return (66);
		else if ( VALOR_TEMP_CALCULAR >= 165 )	
			return (67);
		else if ( VALOR_TEMP_CALCULAR >= 161 )	
			return (68);
		else if ( VALOR_TEMP_CALCULAR >= 156 )
			return (69);
		else if ( VALOR_TEMP_CALCULAR >= 151 )	
			return (70);
	}
	
	else if ( VALOR_TEMP_CALCULAR >= 113 )	//VALOR ADC PARA 71�C a 80�C
	{
		if ( VALOR_TEMP_CALCULAR >= 147 )	
			return (71);
		else if ( VALOR_TEMP_CALCULAR >= 143 )	
			return (72);
		else if ( VALOR_TEMP_CALCULAR >= 139 )
			return (73);
		else if ( VALOR_TEMP_CALCULAR >= 135 )	
			return (74);
		else if ( VALOR_TEMP_CALCULAR >= 131 )	
			return (75);
		else if ( VALOR_TEMP_CALCULAR >= 127 )	
			return (76);
		else if ( VALOR_TEMP_CALCULAR >= 124 )	
			return (77);
		else if ( VALOR_TEMP_CALCULAR >= 120 )	
			return (78);
		else if ( VALOR_TEMP_CALCULAR >= 117 )
			return (79);
		else if ( VALOR_TEMP_CALCULAR >= 113 )	
			return (80);
	}
	
	else if ( VALOR_TEMP_CALCULAR >= 85 )	//VALOR ADC PARA 81�C a 90�C
	{
		if ( VALOR_TEMP_CALCULAR >= 110 )	
			return (81);
		else if ( VALOR_TEMP_CALCULAR >= 107 )	
			return (82);
		else if ( VALOR_TEMP_CALCULAR >= 104 )
			return (83);
		else if ( VALOR_TEMP_CALCULAR >= 101 )	
			return (84);
		else if ( VALOR_TEMP_CALCULAR >= 98 )	
			return (85);
		else if ( VALOR_TEMP_CALCULAR >= 95 )	
			return (86);
		else if ( VALOR_TEMP_CALCULAR >= 93 )	
			return (87);
		else if ( VALOR_TEMP_CALCULAR >= 90 )	
			return (88);
		else if ( VALOR_TEMP_CALCULAR >= 88 )
			return (89);
		else if ( VALOR_TEMP_CALCULAR >= 85 )	
			return (90);
	}
	
	else if ( VALOR_TEMP_CALCULAR >= 65 )	//VALOR ADC PARA 91�C a 100�C
	{
		if ( VALOR_TEMP_CALCULAR >= 83 )	
			return (91);
		else if ( VALOR_TEMP_CALCULAR >= 81 )	
			return (92);
		else if ( VALOR_TEMP_CALCULAR >= 78 )
			return (93);
		else if ( VALOR_TEMP_CALCULAR >= 76 )	
			return (94);
		else if ( VALOR_TEMP_CALCULAR >= 74 )	
			return (95);
		else if ( VALOR_TEMP_CALCULAR >= 72 )	
			return (96);
		else if ( VALOR_TEMP_CALCULAR >= 70 )	
			return (97);
		else if ( VALOR_TEMP_CALCULAR >= 68 )	
			return (98);
		else if ( VALOR_TEMP_CALCULAR >= 66 )
			return (99);
		else if ( VALOR_TEMP_CALCULAR >= 65 )	
			return (100);
	}
	
	else	
		return (101);
	
	return(0);
}
//-------------------------------





