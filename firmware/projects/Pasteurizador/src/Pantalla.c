

#include "Variables.h"
#include "Pantalla.h"


//---------------- VARIABLES Y DEFINICIONES DE PANTALLA-----------------------



uint8_t BUFFER_RX_PANTALLA [20];
uint8_t CONTADOR_RX_PANTALLA = 0;
uint8_t INPUT_RX_PANTALLA = 0;
uint8_t	LONG_PAQUETE_PANTALLA = 0;
uint16_t DATO1=0;
uint16_t DATO2=0;



//DEFINICION COMANDOS DWIN
#define ESCRIBIR_EN_REG			0x80
#define LEER_EN_REG				0x81
#define ESCRIBIR_EN_SRAM		0x82
#define LEER_EN_SRAM			0x83
#define ESCRIBIR_CURVA			0x84


//---------------- FUNCIONES -----------------------

//-------------------------------------------------------------------------------------------------
void procesar_info_Pantalla(void) //RECIBIDA DESDE LA PANTALLA PRINCIPAL DEL EQUIPO (USART1)
{
	FLAG_TOUCH_PRESIONADO = FALSE;

	switch (DATO1)
	{
		case VP_BOTON_GENERAL:
		{
			switch (DATO2)
			{
				case OPCION_PANTALLA_HOME:
				{
					PANTALLA = PANTALLA_HOME;
					CMD_ir_a_pantalla(PANTALLA);
				}break;

				case OPCION_PANTALLA_CHEQUEO_MANUAL:
				{
					PANTALLA = PANTALLA_CHEQUEO_MANUAL;
					CARGAR_PANTALLA_CHEQUEO_MANUAL();
					CMD_ir_a_pantalla(PANTALLA);
				}break;

				case OPCION_CHEQUEO_MANUAL_VOLVER:
				{
					//AL SALIR DEL MODO DE CHEQUEO MANUAL, APAGO TODOS LOS SISTEMAS
					CMD_ir_a_pantalla(PANTALLA_ESPERA);

					SalidaOff(CALDERA);
					SalidaOff(ELECTROVALVULAS);
					SalidaOff(REMOVEDOR);
					SalidaOff(BOMBA_SERVIDORA);
					ESTADO_CALDERA = OFF;
					ESTADO_ELECTROVALVULAS = OFF;
					ESTADO_REMOVEDOR = OFF;
					ESTADO_BOMBA_SERVIDORA = OFF;

					PANTALLA = PANTALLA_HOME;
					CMD_ir_a_pantalla(PANTALLA);
				}break;

				case OPCION_PLAY_STOP:
				{
					if (ESTADO == STOP)	//DE STOP A REGIMEN
					{
						ESTADO = CALENTANDO;

						CONTADOR_ICONO_TANQUE = 1;	//INICIO EL ICONO DE ANIMACION DEL TANQUE CALENTANDO
						CONTADOR_ALARMA_CALENTAMIENTO = 0;	//INICIO EL CONTADOR DE TIEMPO DE ALARMA
						FLAG_FALLA_CALENTAMIENTO = FALSE;
						CONTADOR_SEGUNDOS = 0;

						VALOR_TIEMPO_RETENCION_MINUTOS = VALOR_CONFIG_TIEMPO_RETENCION_MINUTOS;
						VALOR_TIEMPO_RETENCION_SEGUNDOS = VALOR_CONFIG_TIEMPO_RETENCION_SEGUNDOS;

						CARGAR_PANTALLA_PLAY();
						PANTALLA = PANTALLA_PLAY;
						CMD_ir_a_pantalla(PANTALLA);

					}
					else	//PLAY A STOP
					{
						ESTADO = STOP;

						CMD_ir_a_pantalla(PANTALLA_ESPERA);

						SalidaOff(CALDERA);
						SalidaOff(ESTADO_ELECTROVALVULAS);
						SalidaOff(ESTADO_REMOVEDOR);
						SalidaOff(ESTADO_BOMBA_SERVIDORA);
						ESTADO_CALDERA = OFF;
						ESTADO_ELECTROVALVULAS = OFF;
						ESTADO_REMOVEDOR = OFF;
						ESTADO_BOMBA_SERVIDORA = OFF;

						PANTALLA = PANTALLA_HOME;
						CMD_ir_a_pantalla(PANTALLA);

					}
				}break;

				case OPCION_MANUAL_CALDERA:
				{
					if (ESTADO_CALDERA == ON)
					{
						CMD_cargar_valor(VP_ICONO_ESTADO_MANUAL_CALDERA, ICONO_INDICADOR_OFF);
						SalidaOff(CALDERA);
						ESTADO_CALDERA = OFF;
					}
					else
					{
						CMD_cargar_valor(VP_ICONO_ESTADO_MANUAL_CALDERA, ICONO_INDICADOR_ON);
						SalidaOn(CALDERA);
						ESTADO_CALDERA = ON;
					}
				}break;

				case OPCION_MANUAL_ELECTROVALVULAS:
				{
					if (ESTADO_ELECTROVALVULAS == ON)
					{
						CMD_cargar_valor(VP_ICONO_ESTADO_MANUAL_ELECTROVALVULAS, ICONO_INDICADOR_OFF);
						SalidaOff(ELECTROVALVULAS);
						ESTADO_ELECTROVALVULAS = OFF;
					}
					else
					{
						CMD_cargar_valor(VP_ICONO_ESTADO_MANUAL_ELECTROVALVULAS, ICONO_INDICADOR_ON);
						SalidaOn(ELECTROVALVULAS);
						ESTADO_ELECTROVALVULAS = ON;
					}
				}break;

				case OPCION_MANUAL_REMOVEDOR:
				{
					if (ESTADO_REMOVEDOR == ON)
					{
						CMD_cargar_valor(VP_ICONO_ESTADO_MANUAL_REMOVEDOR, ICONO_INDICADOR_OFF);
						SalidaOff(REMOVEDOR);
						ESTADO_REMOVEDOR = OFF;
					}
					else
					{
						CMD_cargar_valor(VP_ICONO_ESTADO_MANUAL_REMOVEDOR, ICONO_INDICADOR_ON);
						SalidaOn(REMOVEDOR);
						ESTADO_REMOVEDOR = ON;
					}
				}break;

				case OPCION_MANUAL_BOMBA_SERVIDORA:
				{
					if (ESTADO_BOMBA_SERVIDORA == ON)
					{
						CMD_cargar_valor(VP_ICONO_ESTADO_MANUAL_BOMBA_SERVIDORA, ICONO_INDICADOR_OFF);	//ICONO EN CHEQUEO MANUAL
						SalidaOff(BOMBA_SERVIDORA);
						ESTADO_BOMBA_SERVIDORA = OFF;
					}
					else
					{
						CMD_cargar_valor(VP_ICONO_ESTADO_MANUAL_BOMBA_SERVIDORA, ICONO_INDICADOR_ON);	//ICONO EN CHEQUEO MANUAL
						SalidaOn(BOMBA_SERVIDORA);
						ESTADO_BOMBA_SERVIDORA = ON;
					}
				}break;

				case OPCION_BOMBA_SERVIDORA_ON_OFF:		//ACTUACION DE LA BOMBA EN FORMA MANUAL EN LA PANTALLA DE PROCESO FINALIZADO
				{
					if (ESTADO == FINAL_BOMBA_OFF)
					{
						ESTADO = FINAL_BOMBA_ON;
						CMD_cargar_valor(VP_ICONO_ESTADO, FINAL_BOMBA_ON);	//ICONO EN ESTADO = FINALIZADO
						SalidaOn (BOMBA_SERVIDORA);
						ESTADO_BOMBA_SERVIDORA = ON;
					}
					else if (ESTADO == FINAL_BOMBA_ON)
					{
						ESTADO = FINAL_BOMBA_OFF;
						CMD_cargar_valor(VP_ICONO_ESTADO, FINAL_BOMBA_OFF);	//ICONO EN ESTADO = FINALIZADO
						SalidaOff (BOMBA_SERVIDORA);
						ESTADO_BOMBA_SERVIDORA = OFF;
					}
				}break;
			}
		}break;

		case VP_OFFSET_TEMPERATURA:
		{
			VALOR_OFFSET_TEMPERATURA = DATO2;
			CMD_cargar_valor(VP_OFFSET_TEMPERATURA, VALOR_OFFSET_TEMPERATURA);
		}break;

		case VP_CONFIG_TEMP_CALENTAMIENTO:
		{
			VALOR_CONFIG_TEMP_CALENTAMIENTO = DATO2;
			CMD_cargar_valor(VP_CONFIG_TEMP_CALENTAMIENTO, VALOR_CONFIG_TEMP_CALENTAMIENTO);
		}break;

		case VP_CONFIG_TEMP_ENFRIAMIENTO:
		{
			VALOR_CONFIG_TEMP_ENFRIAMIENTO = DATO2;
			CMD_cargar_valor(VP_CONFIG_TEMP_ENFRIAMIENTO, VALOR_CONFIG_TEMP_ENFRIAMIENTO);
		}break;

		case VP_CONFIG_TEMP_HISTERISIS:
		{
			VALOR_CONFIG_TEMP_HISTERISIS = DATO2;
			CMD_cargar_valor(VP_CONFIG_TEMP_HISTERISIS, VALOR_CONFIG_TEMP_HISTERISIS);
		}break;

		case VP_CONFIG_TIEMPO_RETENCION_MINUTOS:
		{
			VALOR_CONFIG_TIEMPO_RETENCION_MINUTOS = DATO2;
			CMD_cargar_valor(VP_CONFIG_TIEMPO_RETENCION_MINUTOS, VALOR_CONFIG_TIEMPO_RETENCION_MINUTOS);
		}break;

		case VP_CONFIG_TIEMPO_RETENCION_SEGUNDOS:
		{
			VALOR_CONFIG_TIEMPO_RETENCION_SEGUNDOS = DATO2;
			CMD_cargar_valor(VP_CONFIG_TIEMPO_RETENCION_SEGUNDOS, VALOR_CONFIG_TIEMPO_RETENCION_SEGUNDOS);
		}break;

		case VP_TIEMPO_ALARMA_CALENTAMIENTO:
		{
			VALOR_TIEMPO_ALARMA_CALENTAMIENTO = DATO2;
			CMD_cargar_valor(VP_TIEMPO_ALARMA_CALENTAMIENTO, VALOR_TIEMPO_ALARMA_CALENTAMIENTO);
		}break;

	}

	DATO1 = 0;
	DATO2 = 0;

}//FIN RUTINA DE PROCESAR INFO RECIBIDA

//-------------------------------------------------------------------------------------------------
void CMD_ir_a_pantalla(uint8_t valor )
{

	//DWIN1
	tx_Pantalla( 0x5A );	// Encabezado 1.
	tx_Pantalla( 0xA5 );	// Encabezado 2.
	tx_Pantalla( 0x04 );	// Cantidad de datos.
	tx_Pantalla( ESCRIBIR_EN_REG );
	tx_Pantalla( 0x03 );
	tx_Pantalla( 0x00 );
	tx_Pantalla( valor );

	//DWIN2
/*
	tx_Pantalla( 0x5A );					// Encabezado 1.
	tx_Pantalla( 0xA5 );					// Encabezado 2.
	tx_Pantalla( 0x07 );					// Cantidad de datos.
	tx_Pantalla( ESCRIBIR_EN_SRAM );		//0X80
	tx_Pantalla( 0x00 );					//VP de escribir en pantalla
	tx_Pantalla( 0x84 );					//VP de escribir en pantalla
	tx_Pantalla( 0x5A );
	tx_Pantalla( 0x01 );
	tx_Pantalla( 0x00 );
	tx_Pantalla( valor );
*/
	DelayMs(2);
}

//-------------------------------------------------------------------------------------------------

void CMD_cargar_valor( uint16_t VP, uint16_t valor )
{
	tx_Pantalla( 0x5A );					// Encabezado 1.
	tx_Pantalla( 0xA5 );					// Encabezado 2.
	tx_Pantalla( 0x05 );					// Cantidad de datos.
	tx_Pantalla( ESCRIBIR_EN_SRAM );		//0X82.
	tx_Pantalla( VP >> 8 );
	tx_Pantalla( VP );
	tx_Pantalla( valor >> 8);
	tx_Pantalla( valor );

	DelayMs(2);
}

//-------------------------------------------------------------------------------------------------

void CMD_cargar_valor_4bytes( uint16_t VP, uint32_t valor )
{
	tx_Pantalla( 0x5A );	// Encabezado 1.
	tx_Pantalla( 0xA5 );	// Encabezado 2.
	tx_Pantalla( 0x07 );	// Cantidad de datos.
	tx_Pantalla( ESCRIBIR_EN_SRAM );
	tx_Pantalla( VP >> 8 );
	tx_Pantalla( VP );
	tx_Pantalla( valor >> 24);
	tx_Pantalla( valor >> 16);
	tx_Pantalla( valor >> 8);
	tx_Pantalla( valor );

	DelayMs(2);
}


//-------------------------------------------------------------------------------------------------

void tx_Pantalla( uint8_t byte)
{
	//UartSendByte(SERIAL_PORT_PC, &byte);			//envio el dato AL PUERT DE LA PC...PARA PRUEBAS -*-*- BORRAR -*-*-
	UartSendByte(SERIAL_PORT_P2_CONNECTOR, &byte);	//envio el dato

}

//--------------------------------------------------------------------------------


void atiende_Pantalla(void) // USADA EN IRQ. EN INPUT_RX_DWIN ENTRA EL VALOR DE RCREG1
{
//	uint8_t status_port;
	//status_port = UartReadByte(SERIAL_PORT_P2_CONNECTOR, &INPUT_RX_PANTALLA);

	LedToggle(LED_1);


	if (UartReadByte(SERIAL_PORT_P2_CONNECTOR, &INPUT_RX_PANTALLA) )
	{
		if( CONTADOR_RX_PANTALLA == 0 )
		{																		// 00 - [INICIO]
			if( INPUT_RX_PANTALLA == 0x5A )
			{
				BUFFER_RX_PANTALLA[CONTADOR_RX_PANTALLA] = INPUT_RX_PANTALLA;
				CONTADOR_RX_PANTALLA = 1;
			}
			else
				CONTADOR_RX_PANTALLA = 0;

		}
		else if( CONTADOR_RX_PANTALLA == 1 )
		{																		// 01 - [INICIO]
			if( INPUT_RX_PANTALLA == 0xA5 )
			{
				BUFFER_RX_PANTALLA[CONTADOR_RX_PANTALLA] = INPUT_RX_PANTALLA;
				CONTADOR_RX_PANTALLA = 2;
			}
			else
				CONTADOR_RX_PANTALLA = 0;
		}
		else if( CONTADOR_RX_PANTALLA == 2 )
		{																		// 02 - [LONGITUD]
			BUFFER_RX_PANTALLA[CONTADOR_RX_PANTALLA] = INPUT_RX_PANTALLA;
			LONG_PAQUETE_PANTALLA = INPUT_RX_PANTALLA + 2;							// SE LE SUMAN LOS DOS BYTES DE INICIO
			CONTADOR_RX_PANTALLA = 3;

		}
		else if( CONTADOR_RX_PANTALLA < LONG_PAQUETE_PANTALLA )
		{																		// 01 - [INICIO]
			BUFFER_RX_PANTALLA[CONTADOR_RX_PANTALLA] = INPUT_RX_PANTALLA;
			CONTADOR_RX_PANTALLA++;
		}
		else if( CONTADOR_RX_PANTALLA == LONG_PAQUETE_PANTALLA )				// ULTIMO DATO
		{
			BUFFER_RX_PANTALLA[CONTADOR_RX_PANTALLA] = INPUT_RX_PANTALLA;

			if( CONTADOR_RX_PANTALLA >= 8 )				//LOS PAQUETES DE DATOS NORMALES TIENEN 8 BYTES: (0X5A (INICIO1); 0XA5 (INICIO2); 0x06 (NUMERO BYTES); 0x83 (CMD = DATOS DESDE EL DISPLAY);
			{ 										//VP_H; VP_L; 0x01 (SEPARADOR); KeyValue_H; KeyValue_L )

				if( BUFFER_RX_PANTALLA[3] == LEER_EN_SRAM )
				{
					DATO1 = (BUFFER_RX_PANTALLA[4] << 8) + BUFFER_RX_PANTALLA[5];		// VP DEL BOTON PRESIONADO.
					DATO2 = (BUFFER_RX_PANTALLA[7] << 8) + BUFFER_RX_PANTALLA[8];		// VALOR BOTON PRESIONADO.

					//FLAG_TOUCH_PRESIONADO = TRUEE;	//para procesar la info de la pantalla en el loop principal
					procesar_info_Pantalla();			//para procesar la info de la pantalla dentro de la interrupcion
				}

			}

			CONTADOR_RX_PANTALLA = 0;
		}
		else
			CONTADOR_RX_PANTALLA = 0;
	}
}

//--------------------------------------------------------------------------------







