

/*==================[inclusions]=============================================*/

#include "Pasteurizador.h"

#include "Variables.h"
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "uart.h"
#include "analog_io.h"
#include "timer.h"
#include "Delay.h"
#include "Pantalla.h"
#include "Salidas.h"
#include "Rutinas.h"


/*==================[macros and definitions]=================================*/

timer_config my_timer = {TIMER_A,100,&rutina_TIMER};

serial_config config_serial_PC = {SERIAL_PORT_PC,115200,NO_INT};	//PARA CHEQUEO DE DATOS POR COMPUTADORA
serial_config config_serial_UART = {SERIAL_PORT_P2_CONNECTOR,115200,&atiende_Pantalla};
//serial_config config_serial_UART = {SERIAL_PORT_P2_CONNECTOR,115200,NO_INT};

//analog_input_config my_analog_CH1 = {CH1,ANALOG_INPUT_READ,rutina_ADC_CH1};
analog_input_config my_analog_CH2 = {CH2,ANALOG_INPUT_READ,rutina_ADC_CH2};


uint8_t j;				//GENERO EL BARRIDO EN EL VECTOR DE DATOS ECG1
uint16_t ANALOG_VALUE;	//GUARDO EL VALOR DEL CONVERSOR ADC PARA UTIIZARLO DE MULTIPLICADOR

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
void rutina_ADC_CH1(void)	//RUTINA DE INTERRUPCION DEL ADC CANAL 1
{
	//AnalogInputRead(CH1,&ANALOG_VALUE);		//RECUPERO EL VALOR DEL CANAL 1 DEL CONVERSOR
}
*/

void rutina_ADC_CH2(void)	//RUTINA DE INTERRUPCION DEL ADC CANAL 1
{
	AnalogInputRead(CH2,&ANALOG_VALUE);		//RECUPERO EL VALOR DEL CANAL 1 DEL CONVERSOR

	VALOR_TEMPERATURA = valor_temp_por_tabla (ANALOG_VALUE);
	VALOR_TEMPERATURA = VALOR_TEMPERATURA  + VALOR_OFFSET_TEMPERATURA;

}


//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
void rutina_TIMER(void)
{
	j++;	//INDICE PARA RECORRER EL VECTOR ECG1


	if (j >= 10)				//CONTADOR DE TIEMPO UN SEGUNDO
	{
		LedToggle(LED_RGB_B);	//parpadeo del led AZUL con el periodo de la onda ECG
		j=0;					//reinicio el contador del vector

		//FLAG_PASO_SEGUNDO = TRUEE;	//para procesar las rutinas de 1 segundo en el loop principal
		rutinas_cada_segundo();			//para procesar las rutinas de 1 segundo dentro de la interrupcion
	}

	AnalogStartConvertion();	//inicio una conversion en cada ciclo del timer
								//leo el valor del ADC por interrupcion
}


//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
void SisInit(void)
{
	SystemClockInit();	//INICIALIZO CLOCK

	/* Enable SysTick Timer */
	//SysTick_Config(SystemCoreClock / TICKRATE_HZ);


	LedsInit();			//INICIALIZO LEDS
	//SwitchesInit();	//INICIALIZO BOTONES
	OutputsInit();		//INICIALIZO DE LAS SALIDAS


	TimerInit(&my_timer);				//INICIALIZO EL TIMER CON UNA ESTRUCTURA DE VARIABLES
	UartInit(&config_serial_UART);			//INICIALIZO LA UART DEL CONECTOR (PANTALLA)
	UartInit(&config_serial_PC);			//INICIALIZO LA UART DEL USB DE PC (DEBUG)
	AnalogInputInit(&my_analog_CH2);	//INICIALIZO EL ADC CON UNA ESTRUCTURA DE VARIABLES

	NVIC_EnableIRQ(TIMER1_IRQn);		//HABILITO INTERRUPCION DE TIMER A
	NVIC_ClearPendingIRQ(TIMER1_IRQn);

	NVIC_EnableIRQ(ADC0_IRQn);			//HABILITO INTERRUPCION DE ADC

	TimerStart(TIMER_A);	//INICIO TIMER A

}


//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
int main(void)
{
	SisInit();	//INICIALIZCION DE LOS COMPONENTES DEL SISTEMA

	LedOn(LED_3);

	CMD_ir_a_pantalla(0);	//PANTALLA INICIO
	DelaySec(5);			//ESPERA MIENTRAS SE REALIZA LA ANIMACION DE PRESENTACION
	CMD_ir_a_pantalla(PANTALLA_HOME);	//
	CMD_cargar_valor(VP_VERSION_FW,VALOR_VERSION_FW);
	INICIALIZACION_VALORES();

	while(1)	//LOOP PRINCIPAL
    {
		//if (FLAG_TOUCH_PRESIONADO == TRUEE) 	procesar_info_Pantalla();
		//if (FLAG_PASO_SEGUNDO == TRUEE) 		rutinas_cada_segundo();
    }

}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------


/*==================[end of file]============================================*/


