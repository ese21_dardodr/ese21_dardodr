

#ifndef PASTEURIZADOR_H
#define PASTEURIZADOR_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/

//---------------- DEFINICION DE FUNCIONES -----------------------


void rutina_TIMER(void);
void rutina_UART(void);
void rutina_ADC_CH1(void);
void rutina_ADC_CH2(void);


#endif /* PASTEURIZADOR_H_ */



