
//==================[inclusions]=============================================
#include <stdint.h>
#include "Variables.h"
#include "uart.h"
#include "Delay.h"
#include "Salidas.h"
#include "led.h"

#ifndef PANTALLA_H
#define PANTALLA_H


//---------------- DEFINICION DE FUNCIONES -----------------------

// SALTO A UNA PANTALLA DETERMINADA
void CMD_ir_a_pantalla(uint8_t);

//CARGO UN VALOR DE 2 BYTES EN UNA DETERMINADA DIRECCION VP
void CMD_cargar_valor( uint16_t VP, uint16_t dato );

//CARGO UN VALOR DE 4 BYTES EN UNA DETERMINADA DIRECCION VP
void CMD_cargar_valor_4bytes( uint16_t VP, uint32_t dato );

//ATENCION DE INTERRUPCION DE RECEPCION
void atiende_Pantalla(void);

//TRANSMITO EL DATO A LA PANTALLA
void tx_Pantalla( uint8_t byte);


void procesar_info_Pantalla(void);








#endif /* PANTALLA_H_ */









