

/*==================[inclusions]=============================================*/
#include "ECG_Control.h"   
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "uart.h"
#include "analog_io.h"
#include "timer.h"

/*==================[macros and definitions]=================================*/

void rutina_TIMER(void);
void rutina_UART(void);
void rutina_ADC_CH1(void);
void rutina_ADC_CH2(void);

#define ON 1
#define OFF 0

timer_config my_timer = {TIMER_A,5,&rutina_TIMER};

//serial_config my_serial_INT = {SERIAL_PORT_PC,115200,&rutina_UART};
serial_config my_serial_NO_INT = {SERIAL_PORT_PC,115200,};

analog_input_config my_analog_CH1 = {CH1,ANALOG_INPUT_READ,rutina_ADC_CH1};
//analog_input_config my_analog_CH2 = {CH2,ANALOG_INPUT_READ,rutina_ADC_CH2};


uint8_t j;				//GENERO EL BARRIDO EN EL VECTOR DE DATOS ECG1
uint16_t ANALOG_VALUE;	//GUARDO EL VALOR DEL CONVERSOR ADC PARA UTIIZARLO DE MULTIPLICADOR
uint8_t DIVISOR;		//UTILIZADO PARA DIVIDIR LOS VALORES DE LA SEÑAL ECG1
uint8_t UART_DATA_TX;

const uint8_t ECG1[]={
		76, 77, 78, 77, 79, 86, 81, 76, 84, 93, 85, 80,
		89, 95, 89, 85, 93, 98, 94, 88, 98, 105, 96, 91,
		99, 105, 101, 96, 102, 106, 101, 96, 100, 107, 101,
		94, 100, 104, 100, 91, 99, 103, 98, 91, 96, 105, 95,
		88, 95, 100, 94, 85, 93, 99, 92, 84, 91, 96, 87, 80,
		83, 92, 86, 78, 84, 89, 79, 73, 81, 83, 78, 70, 80, 82,
		79, 69, 80, 82, 81, 70, 75, 81, 77, 74, 79, 83, 82, 72,
		80, 87, 79, 76, 85, 95, 87, 81, 88, 93, 88, 84, 87, 94,
		86, 82, 85, 94, 85, 82, 85, 95, 86, 83, 92, 99, 91, 88,
		94, 98, 95, 90, 97, 105, 104, 94, 98, 114, 117, 124, 144,
		180, 210, 236, 253, 227, 171, 99, 49, 34, 29, 43, 69, 89,
		89, 90, 98, 107, 104, 98, 104, 110, 102, 98, 103, 111, 101,
		94, 103, 108, 102, 95, 97, 106, 100, 92, 101, 103, 100, 94, 98,
		103, 96, 90, 98, 103, 97, 90, 99, 104, 95, 90, 99, 104, 100, 93,
		100, 106, 101, 93, 101, 105, 103, 96, 105, 112, 105, 99, 103, 108,
		99, 96, 102, 106, 99, 90, 92, 100, 87, 80, 82, 88, 77, 69, 75, 79,
		74, 67, 71, 78, 72, 67, 73, 81, 77, 71, 75, 84, 79, 77, 77, 76, 76,
};


//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
void rutina_ADC_CH1(void)	//RUTINA DE INTERRUPCION DEL ADC CANAL 1
{
	AnalogInputRead(CH1,&ANALOG_VALUE);		//RECUPERO EL VALOR DEL CANAL 1 DEL CONVERSOR

	//CALCULO UNA VARIABLE DE DIVISION QUE DEPENDE DIRECTAMENTE DEL VALOR ANALOGICO
	if (ANALOG_VALUE >= 102)	DIVISOR = (uint8_t) (ANALOG_VALUE / 102);	//ME ASEGURO QUE EL VALOR ANALOGICO SEA MAYOR A 102 PARA PODER DIVIDIR
	else						DIVISOR = 1;					//SINO, DEFINO DIVISOR COMO 1
																//PASOS de 1 a 10 SEGUN ANALOG_VALUE (Ej: 1023/102=10; 512/102=5; 256/102=2; 102/102=1)
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
void rutina_ADC_CH2(void)	//RUTINA DE INTERRUPCION DEL ADC CANAL 2
{
}
*/

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
void rutina_TIMER(void)
{
	j++;	//INDICE PARA RECORRER EL VECTOR ECG1

	UART_DATA_TX = ( ECG1[j] / DIVISOR);			//ajusto el valor del dato ECG1[j] dividiendolo por "DIVISOR"
	UartSendByte(SERIAL_PORT_PC, &UART_DATA_TX );	//envio el dato guardado en UART_DATA_TX

	if (j >= 230)				//longitud del vector ECG1
	{
		LedToggle(LED_RGB_B);	//parpadeo del led AZUL con el periodo de la onda ECG
		j=0;					//reinicio el contador del vector
	}


	AnalogStartConvertion();	//inicio una conversion en cada ciclo del timer
								//leo el valor del ADC por interrupcion
}


//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
void rutina_UART(void) //rutina de interrupcion de la UART
{
	LedToggle(LED_RGB_R);
}
*/


//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
void SisInit(void)
{
	SystemClockInit();	//INICIALIZO CLOCK

	LedsInit();			//INICIALIZO LEDS
	//SwitchesInit();	//INICIALIZO BOTONES

	TimerInit(&my_timer);				//INICIALIZO EL TIMER CON UNA ESTRUCTURA DE VARIABLES
	UartInit(&my_serial_NO_INT);		//INICIALIZO LA UART CON UNA ESTRUCTURA DE VARIABLES SIN INTERRUPCION
	AnalogInputInit(&my_analog_CH1);	//INICIALIZO EL ADC CON UNA ESTRUCTURA DE VARIABLES

	NVIC_EnableIRQ(TIMER1_IRQn);		//HABILITO INTERRUPCION DE TIMER A
	NVIC_ClearPendingIRQ(TIMER1_IRQn);

	NVIC_EnableIRQ(ADC0_IRQn);			//HABILITO INTERRUPCION DE ADC

	TimerStart(TIMER_A);	//INICIO TIMER A
}


//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
int main(void)
{
	SisInit();	//INICIALIZCION DE LOS COMPONENTES DEL SISTEMA

	LedOn(LED_3);


	while(1)	//LOOP INFINITO
    {

    }

}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------


/*==================[end of file]============================================*/


