#ifndef SALIDAS_H
#define SALIDAS_H



/*==================[inclusions]=============================================*/
#include "bool.h"
#include <stdint.h>

/*==================[macros]=================================================*/
#define lpc4337            1
#define mk60fx512vlq15     2

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/
/** @brief Definition of constants to reference the EDU-CIAA leds.
 */
//enum SALIDA_COLOR {CALDERA=(1<<0), ELECTROVALVULAS=(1<<1), REMOVEDOR=(1<<2) };


#define CALDERA 		1
#define ELECTROVALVULAS 3
#define REMOVEDOR 		5
#define BOMBA_SERVIDORA 7

uint8_t	ESTADO_CALDERA;
uint8_t	ESTADO_ELECTROVALVULAS;
uint8_t	ESTADO_REMOVEDOR;
uint8_t	ESTADO_BOMBA_SERVIDORA;


/** \brief Definition of constants to control the EDU-CIAA leds.
 **
 **/


/*==================[external functions declaration]=========================*/

/** @fn void LedsInit(void)
 * @brief Initialization function of EDU-CIAA leds
 * Mapping ports (PinMux function), set direction and initial state of leds ports
 * @param[in] No parameter
 * @return TRUE if no error
 */
void SalidasInit(void);


/** @fn uint8_t LedOn(uint8_t led)
 * @brief Function to turn on a specific led
 * @param[in] led
 * @return FALSE if an error occurs, in other case returns TRUE
 */
void SalidaOn(uint8_t led);

/** @fn uint8_t LedOff(uint8_t led)
 * @brief Function to turn off a specific led
 * @param[in] led
 * @return FALSE if an error occurs, in other case returns TRUE
 */
void SalidaOff(uint8_t led);

/** @fn uint8_t LedToggle(uint8_t led)
 * @brief Function to toggle a specific led
 * @param[in] led
 * @return FALSE if an error occurs, in other case returns TRUE
 */
/*==================[end of file]============================================*/
#endif /* #ifndef LED_H */

