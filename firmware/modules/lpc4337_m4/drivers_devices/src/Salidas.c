

/*==================[inclusions]=============================================*/
#include "Salidas.h"
#include "gpio.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/




/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/


void OutputsInit(void)
{
	/** Configuration of the GPIO */
	GPIOInit(GPIO_1, GPIO_OUTPUT);
	GPIOInit(GPIO_3, GPIO_OUTPUT);
	GPIOInit(GPIO_5, GPIO_OUTPUT);
	GPIOInit(GPIO_7, GPIO_OUTPUT);

	/** Turn off Outpuss*/
	GPIOOff(CALDERA);
	GPIOOff(ELECTROVALVULAS);
	GPIOOff(REMOVEDOR);
	GPIOOff(BOMBA_SERVIDORA);
}


/** \brief Function to turn on a specific led */
void SalidaOn(uint8_t salida)
{
	switch (salida)
	{
	case CALDERA:
		GPIOOn(GPIO_1);
		break;
	case ELECTROVALVULAS:
		GPIOOn(GPIO_3);
		break;
	case REMOVEDOR:
		GPIOOn(GPIO_5);
		break;
	case BOMBA_SERVIDORA:
		GPIOOn(GPIO_7);
		break;
	}
}

void SalidaOff(uint8_t salida)
{
	switch (salida)
	{
	case CALDERA:
		GPIOOff(GPIO_1);
		break;
	case ELECTROVALVULAS:
		GPIOOff(GPIO_3);
		break;
	case REMOVEDOR:
		GPIOOff(GPIO_5);
		break;
	case BOMBA_SERVIDORA:
		GPIOOff(GPIO_7);
		break;
	}
}

/*==================[end of file]============================================*/
