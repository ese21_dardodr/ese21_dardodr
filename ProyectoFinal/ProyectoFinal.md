# Sistema Automatico de Pasteurizacion de leche ESE

## Indice

<a name="Intro"></a>
## - Introduccion
<a name="Componentes"></a>
## - Componentes
<a name="Guia de Usuario"></a>
## - Guia de Usuario
<a name="Electrónica"></a>
## - Electronica
<a name="Fotos y Videos"></a>
## - Fotos y Videos

***

## Introduccion:

<details><summary>Click para expandir</summary>

- La funcion del sistema es realizar automaticamente el proceso de pasteurizacion de leche y/o complementos alimenticios en un tanque para dejarlos listos para el consumo de terneros en galpones de cria.

- El sistema controlara la temperatura y tiempos del proceso de pasteurizacion.


</details>

***

## - Componentes

<details><summary>Click para expandir</summary>
 
- Placa electronica de control modelo EDU-CIAA
- Display HMI con pantalla Táctil
- Sensor de temperatura NTC 10K
- Placa adaptadora de control (Placa Poncho)
- Fuente de alimentación Switching 12V 2Amp
- Extras: Gabinete, cables, borneras, Tornilleria, etc.
Notas: Tanto el tanque de acero inoxidable como los sistemas de calefaccion, bombas, tanques, etc. son desarrollados y fabricados por la propia metalurgica.

<br>
![DEB General](/ProyectoFinal/Docs/DEB Gral.bmp)
<br>
![DEB Electrico](/ProyectoFinal/Docs/Diagrama electrico.bmp)
<br>
![DEB Mecanico](/ProyectoFinal/Docs/Diagrama mecanico.bmp)



</details>

***
## - Guia de Usuario

<details><summary>Click para expandir</summary>
Guia de usuario
<br>
![Guia de Usuario](/ProyectoFinal/Docs/Guia PasteurizadorESE.pdf)
 

</details>

***

## - Electronica

<details><summary>Click para expandir</summary>
Diagrama Esquemático y Topográfico
<br>
![Esquematico y Topograficos](/ProyectoFinal/Docs/PlacaESE_V1.1.pdf)

</details>

***

## Fotos y Videos

<details><summary>Click para expandir</summary>
Foto Placa Poncho 
![Placa Poncho Prototipo](/ProyectoFinal/Docs/Prototipo.jpg)

<br>
Videos de funcionamiento
<br>
![Explicacion Placa](/ProyectoFinal/Docs/Conformacion de la placa.mp4)
<br>
![Pantalla incio](/ProyectoFinal/Docs/Pantalla Inicio.mp4)
<br>
![Pantalla chequeo manual](/ProyectoFinal/Docs/Pantalla Chequeo Manual.mp4)
<br>
![Pantalla funcionamiento](/ProyectoFinal/Docs/Pantalla Funcionamiento.mp4)

</details>



